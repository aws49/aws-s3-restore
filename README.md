# AWS S3 Restore Objects

## About

**AWS S3 Restore Objects via AWS CLI**

**Script:** [`s3-restore.sh`](s3-restore.sh)  
**AWS CLI:** `version 2`  
**Language:** `Bash`

This is script gives the automated process of restoring objects by deleting all Delete Markers in the bucket
and synchronizing objects from a replication bucket(if it exists).

**Author:** `Oleksii Pavliuk` __(pavliuk.aleksey@gmail.com)__  
**Date:** `07/08/2020`

`[!]` The script wrote on the Mac OS system and can have undefined behavior
      on another operating system.

## Features
There are some scripts features which you need to know!

The script does:
- find and delete all Delete Markers in the bucket;
- check the replication bucket rule and get name of the replication bucket (if exists);
- synchronize buckets (the main bucket & the replication bucket) and compare its objects;
- copies objects which absent in the main bucket but exist in the replication bucket.

[!] The script `Disable` replication rule before synchronizing buckets and after 
    this is `Enable` again. It does for avoiding replication of the same objects 
    to replication bucket again. 

## Before using the script

Before using this script:
- sure that you have full-access permissions to **AWS S3**;
- [install](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) and [configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) **AWS CLI version 2**.

## Usage
```shell script
Usage: ./s3-restore.sh [OPTIONS]
  OPTIONS:
     [required]:
        --bucket ARG                  : a name of bucket
     [optional]:
        --aws-region ARG              : AWS Region [default: $AWS_REGION]
        --cli-profile ARG             : profile in AWS CLI [default: $CLI_PROFILE]
        --log-file-path ARG           : path to log file for logging [default: $LOG_FILE_PATH]
        --without-approve             : do all processes without user approve:
                                          * deleting all Delete Markers;
                                          * synchronizing buckets objects;
                                          * continue the process of synchronizing objects   
                                            even replication rule is Disabled.
        -h | --help                   : show this usage

Example: ./s3-restore.sh --bucket BUCKET_NAME
```
